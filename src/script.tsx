import React from 'react';
import { createRoot } from 'react-dom/client';
import './common.css';
import { App } from './Components/App/App';

document.body.innerHTML = '<div id="app"></div>';

const app = document.getElementById('app');

if (!app) {
  throw new Error('App root element not found');
} else {
  const root = createRoot(app);
  root.render(<App />);
}
